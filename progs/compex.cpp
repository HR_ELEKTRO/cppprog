#include <iostream>
#include <complex>
using namespace std;

int main() {
    complex<double> c1{1, 2};
    cout << c1 + 2i << '\n';
}