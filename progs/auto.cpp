#include <iostream>
using namespace std;

int main() {
    int kampioensjaren_feyenoord[] {1924, 1928, 1936, 1938, 1940, 1961, 1962, 1965, 1969, 1971, 1974, 1984, 1993, 1999, 2017, 2023};

    auto eerste {kampioensjaren_feyenoord[0]};

    cout << "Feyenoord was voor het eerst landskampioen in: " << eerste;
}