#include <iostream>
#include <array>
#include <iomanip>
using namespace std;

struct Tijdsduur { // Een Tijdsduur bestaat uit:
    int uur;       //    een aantal uren en
    int minuten;   //    een aantal minuten.
};

using Rij = array<Tijdsduur, 5>;

// Deze functie drukt een Tijdsduur af
void drukaf(Tijdsduur td) {
    if (td.uur == 0)
        cout<<"           ";
    else
        cout << setw(3) << td.uur << " uur en ";
    cout << setw(2) << td.minuten << " minuten" << '\n';
}

// Deze functie drukt een rij met Tijdsduren af
void drukaf(const Rij& rij) {
    for (auto t: rij)
        drukaf(t);
}

// Deze functie berekent de totale Tijdsduur van een rij met Tijdsduren
auto som(const Rij& rij) {
    Tijdsduur s{0, 0};
    for (auto t: rij) {
        s.uur += t.uur;
        s.minuten += t.minuten;
    }
    s.uur += s.minuten / 60;
    s.minuten %= 60;
    return s;
}

int main() {
    Rij tijdsduren;
    Rij::size_type aantal {0};
    do {
        cout << "Type uren en minuten in: ";
        cin >> tijdsduren[aantal].uur >> tijdsduren[aantal].minuten;
    }
    while (cin && ++aantal < tijdsduren.size());
    cout << '\n';
    drukaf(tijdsduren);
    cout << "De totale tijdsduur is:\n";
    drukaf(som(tijdsduren));
}
