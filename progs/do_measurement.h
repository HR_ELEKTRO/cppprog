#ifndef _do_measurement_
#define _do_measurement_
#include "Adccard.h"

void do_measurement(ADCCard& card, double factor, int channel);
#endif
